﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class KeyPickUp : MonoBehaviour
{
    public TriggerListener trigger;
    public Image cursorImage;
    public Text screenText;
    public AudioSource audioSource;
    MeshRenderer meshRend;
    public bool keyPickedUp = false;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();
    }

    void OnMouseOver()
    {
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
                screenText.text = "Key";
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseExit()

    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {
            audioSource.Play();
            meshRend.enabled = false;
            keyPickedUp = true;
            Invoke("KeyAquiredText", 0.02f);
            Invoke("BlankText", 2f);
            GetComponent<SphereCollider>().enabled = false;
        }
    }

    void KeyAquiredText()
    {
        screenText.text = "Key Aquired";
    }

    void BlankText()
    {
        screenText.text = null;
    }
}



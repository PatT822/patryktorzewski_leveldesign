﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class GameManager : MonoBehaviour
{
    public Text narrativeText;

    // Use this for initialization
    void Start()
    {
        Invoke("IntroText1", 3);
    }

    void IntroText1()
    {
        narrativeText.text = "Escape the room";
        Invoke("IntroText2", 3);
    }

}
